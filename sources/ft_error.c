/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/30 10:36:18 by akazian           #+#    #+#             */
/*   Updated: 2013/12/31 13:23:49 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <stdlib.h>

int	pipex_error(char *er, char *file)
{
	ft_putstr_fd("pipex: ", 2);
	if (file)
	{
		ft_putstr_fd(er, 2);
		ft_putstr_fd(": ", 2);
		ft_putendl_fd(file, 2);
	}
	else
		ft_putendl_fd(er, 2);
	exit(1);
	return (1);
}
