/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/30 10:30:00 by akazian           #+#    #+#             */
/*   Updated: 2013/12/31 13:33:26 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <pipex.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>

static char	*value_test(char *value, char **split, char **envp)
{
	if ((value = value_finder(split[0], (const char **) envp)) == NULL)
		pipex_error("command not found: ", split[0]);
	return (value);
}

static void	child_mol(int *fd, char **split, char **envp, char **argv)
{
	char	*value = NULL;

	value = value_test(value, split, envp);
	close(fd[1]);
	dup2(fd[0], 0);
	close(fd[0]);
	if ((fd[0] = open(argv[4], O_RDWR | O_CREAT | O_TRUNC, S_IRUSR 
				| S_IWUSR)) < 0)
		pipex_error("can't open infile", argv[1]);
	dup2(fd[0], 1);
	close(fd[0]);
	execve(ft_strcat(value, split[0]), split, envp);
	exit(0);
}

static void	 norm_is_dup(int *fd)
{
	dup2(fd[0], 0);
	dup2(fd[1], 1);
	close(fd[0]);
	close(fd[1]);
}

static int	exec(char **argv, char **envp)
{
	char	*value = NULL;
	pid_t	pid;
	int		fd[2];
	char	**split;

	if ((pipe(fd) == -1) && (pipex_error("pipe failled", NULL)))
		return (1);
	split = ft_strsplit(argv[3], ' ');
	if ((pid = fork()) < 0 && (pipex_error("Fork not allowed", NULL)))
		return (1);
	if (pid == 0)
		child_mol(fd, split, envp, argv);
	else
	{
		split = ft_strsplit(argv[2], ' ');
		value = value_test(value, split, envp);
		close(fd[0]);
		if ((fd[0] = open(argv[1], O_RDONLY)) == -1)
			pipex_error("can't open infile", argv[1]);
		norm_is_dup(fd);
		execve(ft_strcat(value, split[0]), split, envp);
		wait(NULL);
	}
	return (0);
}

int			main(int argc, char **argv, char **envp)
{
	if (argc < 5)
		return (pipex_error("Too few arguments", NULL));
	else if (argc > 5)
		return (pipex_error("Too many arguments", NULL));
	exec(argv, envp);
	return (0);
}
