/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   value_finder.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/15 10:57:44 by aeddi             #+#    #+#             */
/*   Updated: 2013/12/30 11:11:49 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <pipex.h>
#include <libft.h>
#include <unistd.h>

void		del_double_char(char **str)
{
	size_t	i;

	i = 0;
	while (str[i])
	{
		ft_strdel(&str[i]);
		i++;
	}
	ft_strdel(str);
}

char	*value_finder(char *line, const char **envp)
{
	char	*value = NULL;
	char	**path;
	size_t	i;
	char	*test_a;

	i = 0;
	while (envp[i] && (ft_strncmp(envp[i], "PATH=", 5)))
		i++;
	if (envp[i] == '\0')
		return (NULL);
	path = ft_strsplit(envp[i] + 5, ':');
	i = 0;
	while (path[i])
	{
		value = ft_strjoin(path[i], "/");
		test_a = ft_strjoin(value, line);
		if (!access(test_a, F_OK || X_OK))
			break ;
		ft_strdel(&value);
		ft_strdel(&test_a);
		i++;
	}
	ft_strdel(&test_a);
	del_double_char(path);
	return (value);
}
