# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: akazian <akazian@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/16 16:00:35 by akazian           #+#    #+#              #
#    Updated: 2013/12/30 11:47:44 by akazian          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME			=	pipex
CC				=	gcc
CFLAGS			=	-Wall -Wextra -Werror -g3
LIBFTDIR		=	./libft/
LIBFTH			=	-I$(LIBFTDIR)
LIBFTFLAGS		=	-L$(LIBFTDIR) -lft
INCS_DIR		=	includes
OBJS_DIR		=	objects
SRCS_DIR		=	sources
SRCS			=	main.c\
					ft_error.c\
					value_finder.c

OBJS 			=	$(patsubst %.c, $(OBJS_DIR)/%.o, $(SRCS))

all				:	$(NAME)

$(NAME)			:	create_objs_dir $(OBJS)
	$(CC) -o $(NAME) $(OBJS) $(LIBFTFLAGS)

$(OBJS_DIR)/%.o	:	$(addprefix $(SRCS_DIR)/, %.c)
	$(CC) $(CFLAGS) -o $@ -c $^ -I $(INCS_DIR) $(LIBFTH)

create_objs_dir	:	makelibft
	mkdir -p $(OBJS_DIR)

makelibft:
	make -C $(LIBFTDIR)

fclean			:	clean
	rm -f $(NAME)

clean			:
	rm -rf $(OBJS_DIR)

re				:	fclean all

.PHONY: clean all re fclean

