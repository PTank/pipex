/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 19:40:13 by akazian           #+#    #+#             */
/*   Updated: 2013/12/01 15:48:40 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

char	*ft_strncpy(char *s1, const char *s2, size_t n)
{
	char	*cp;

	cp = s1;
	while (n > 0)
	{
		if (*s2 != '\0')
			*cp++ = *(char *)s2++;
		else
			*cp++ = '\0';
		n--;
	}
	return (s1);
}
