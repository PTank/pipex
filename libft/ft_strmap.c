/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/24 16:39:07 by akazian           #+#    #+#             */
/*   Updated: 2013/12/01 20:10:49 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*fstr;
	size_t	i;

	i = 0;
	if (s && f)
	{
		fstr = ft_strnew(ft_strlen(s));
		while (s[i] != '\0')
		{
			fstr[i] = f(s[i]);
			i++;
		}
	}
	else
		return (NULL);
	return (fstr);
}
