/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/30 10:42:51 by akazian           #+#    #+#             */
/*   Updated: 2013/12/30 11:54:21 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _PIPEX_H
# define _PIPEX_H

# include <stdlib.h>

char	*value_finder(char *line, const char **envp);
void	del_double_char(char **str);

int	pipex_error(char *er, char *file);

#endif
